/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: VariableLuminosity.cpp,v 1.6 2009-04-07 16:11:21 gcorti Exp $
// Include files

// local
#include "VariableLuminosity.h"

// From CLHEP
#include "CLHEP/Units/SystemOfUnits.h"

// From Event
#include "Event/BeamParameters.h"
#include "Event/GenFSR.h"
#include "Event/GenFSRMTManager.h"
#include "Event/GenCountersFSR.h"

// From Generators
#include "Generators/GenCounters.h"
#include "GenInterfaces/ICounterLogFile.h"

#include "CLHEP/Random/RandomEngine.h"
#include "CLHEP/Random/RandPoisson.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VariableLuminosity
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( VariableLuminosity )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VariableLuminosity::VariableLuminosity( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : GaudiTool ( type, name , parent ) ,
    m_xmlLogTool( 0 ) ,
    m_numberOfZeroInteraction( 0 ) ,
    m_nEvents( 0 ) {

    using CLHEP::s;
    declareInterface< IPileUpTool >( this ) ;
    declareProperty ( "GenFSRLocation", m_FSRName =
                      LHCb::GenFSRLocation::Default);
    declareProperty( "BeamParameters" ,
                     m_beamParameters = LHCb::BeamParametersLocation::Default ) ;
    declareProperty ( "FillDuration"  , m_fillDuration  = 7.0 * 3600 * s    ) ;
    declareProperty ( "BeamDecayTime" , m_beamDecayTime = 10.0 * 3600 * s   ) ;
}

//=============================================================================
// Destructor
//=============================================================================
VariableLuminosity::~VariableLuminosity( ) { ; }

//=============================================================================
// Initialize method
//=============================================================================
StatusCode VariableLuminosity::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;

  // XML file
  m_xmlLogTool = tool< ICounterLogFile >( "XmlCounterLogFile" ) ;

  using CLHEP::s;
  info() << "Poisson distribution with 'LHCb mean'. " << endmsg ;
  info() << "Fill duration (hours): " << m_fillDuration / 3600 / s << endmsg ;
  info() << "Beam decay time (hours): " << m_beamDecayTime / 3600 / s
         << endmsg ;

  return sc ;
}

//=============================================================================
// Compute the number of pile up to generate according to beam parameters
//=============================================================================
unsigned int VariableLuminosity::numberOfPileUp( HepRandomEnginePtr & engine) {
  LHCb::BeamParameters * beam = get< LHCb::BeamParameters >( m_beamParameters ) ;
  if ( 0 == beam ) Exception( "No beam parameters registered" ) ;

  auto genFSR = GenFSRMTManager::GetGenFSR(m_FSRName);

  unsigned int result = 0 ;
  double mean , currentLuminosity;
  while ( 0 == result ) {
    m_nEvents++ ;
    if(genFSR) {
      genFSR->incrementGenCounter(LHCb::GenCountersFSR::CounterKey::AllEvt, 1);
    }
    currentLuminosity = beam -> luminosity() * m_fillDuration / m_beamDecayTime /
      ( 1.0 - exp( -m_fillDuration / m_beamDecayTime ) ) ;

    mean = currentLuminosity * beam -> totalXSec() / beam -> revolutionFrequency() ;
    CLHEP::RandPoisson poissonGenerator{engine.getref(), mean};
    result = (unsigned int) poissonGenerator() ;
    if ( 0 == result ) {
      m_numberOfZeroInteraction++ ;
      if(genFSR) {
        genFSR->incrementGenCounter(LHCb::GenCountersFSR::CounterKey::ZeroInt, 1);
      }
    }
  }
  
  return result ;
}

//=============================================================================
// Print the specific pile up counters
//=============================================================================
void VariableLuminosity::printPileUpCounters( ) {
  using namespace GenCounters ;
  printCounter( m_xmlLogTool , "all events (including empty events)" , m_nEvents ) ;
  printCounter( m_xmlLogTool , "events with 0 interaction" ,
                m_numberOfZeroInteraction ) ;
}

//=============================================================================
// Finalize method
//=============================================================================
StatusCode VariableLuminosity::finalize( ) {
  return GaudiTool::finalize( ) ;
}
