/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: SignalForcedFragmentation.h,v 1.6 2008-05-29 14:22:00 gcorti Exp $
#ifndef GENERATORS_SIGNALFORCEDFRAGMENTATION_H 
#define GENERATORS_SIGNALFORCEDFRAGMENTATION_H 1

// Include files
#include "Generators/Signal.h"
#include "GaudiKernel/Transform4DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

/** @class SignalForcedFragmentation SignalForcedFragmentation.h "SignalForcedFragmentation.h"
 *  
 *  Tool for signal generation with forced fragmentation.
 *  Concrete implementation of ISampleGenerationTool using
 *  the Signal base class.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-18
 */
class SignalForcedFragmentation : public Signal {
 public:
  /// Standard constructor
   SignalForcedFragmentation( const std::string& type, const std::string& name, const IInterface* parent )
       : Signal( type, name, parent ), m_signalMass( 0. )
   {
   }

   virtual ~SignalForcedFragmentation() = default; ///< Destructor

   virtual StatusCode initialize() override; ///< Initialize

   /** Generate set of interactions.
    *  Implements ISampleGenerationTool::generate
    *  -# Choose randomly a flavour to generate for each event
    *  -# Generate with the IDecayTool the signal event (at rest)
    *     of this flavour.
    *  -# Force the fragmentation into this flavour when an
    *     interaction contains a b quark, calling
    *     IProductionTool::setupForcedFragmentation.
    */
   virtual bool generate( const unsigned int nPileUp, HepMC3::GenEventPtrs& theEvents,
                          LHCb::GenCollisions& theCollisions , HepRandomEnginePtr & engine ) const override;

 private:
   /** Boost a particle at rest in the lab frame.
    *  @param[in,out] theSignal       Particle in the lab. frame
    *                                 to boost to
    *  @param[in]     theSignalAtRest Decay tree at rest to boost
    *  @param[in]     theVector       3-momentum boost vector
    */
   StatusCode boostTree( HepMC3::GenParticlePtr theSignal, HepMC3::ConstGenParticlePtr theSignalAtRest,
                         const ROOT::Math::Boost& theBoost ) const;

   double m_signalMass = 0; ///< Mass of the signal particle
};


#endif // GENERATORS_SIGNALFORCEDFRAGMENTATION_H
