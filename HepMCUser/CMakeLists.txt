###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: HepMCUser
#
#################################################################################

gaudi_subdir(HepMCUser v1r0)

gaudi_depends_on_subdirs(Defaults
                         GaudiAlg
                         HepMC3)

gaudi_install_headers(HepMCUser)

find_package(ROOT)
AddHepMC3()

gaudi_add_library(HepMCUtils
                  src/utils/*.cpp
                  PUBLIC_HEADERS HepMCUtils
                  INCLUDE_DIRS HepMCUser Defaults ${RANGEV3_INCLUDE_DIRS} ROOT
                  LINK_LIBRARIES ${HEPMC3_LIBRARIES} GaudiAlgLib ROOT PartPropLib)

#gaudi_add_library(HepMCUser
                  #src/user/*.cpp
                  #PUBLIC_HEADERS HepMCUtils
                  #INCLUDE_DIRS HepMCUser Defaults ${RANGEV3_INCLUDE_DIRS} ROOT
                  #LINK_LIBRARIES ${HEPMC3_LIBRARIES} GaudiAlgLib ROOT)

# FIXME: Force the external build before the Utils here. Hack it time
add_dependencies(HepMCUtils HepMC3Ext)

gaudi_add_executable(compareHepMCEvents
                     exec/*.cpp
                     LINK_LIBRARIES HepMCUtils)
