.. Gaussino documentation master file, created by
   sphinx-quickstart on Fri Apr 30 15:52:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gaussino's documentation! testing2
====================================
.. toctree::
   :caption: About
   :hidden:
   :maxdepth: 2

   about/gaussino

.. toctree::
   :caption: Getting started
   :hidden:
   :maxdepth: 2

   getting_started/installing.md
   getting_started/contributing.md

.. toctree::
   :caption: Configuration
   :maxdepth: 2

   configuration/gaussino
   configuration/generation
   configuration/simulation
   configuration/external_detector
   configuration/parallel_geometry

.. toctree::
   :caption: Examples
   :hidden:
   :maxdepth: 2
   
   examples/external_detector
   examples/parallel_geometry


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
