# Gaussino
Main configuration class


```{eval-rst}
.. currentmodule:: Gaussino.Configuration

.. autoclass:: Gaussino
   :show-inheritance:
   :members: setOtherProp, setOtherProps, setupHive, outputName, co
   :undoc-members:
   :special-members: __apply_configuration__
```
