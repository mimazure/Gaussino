###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "COPYING".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function


def configure_pgun(**kwargs):
    """Simple utility function to create and configure an instance of particle
    gun

    :**kwargs: Optional keyword arguments (not curently used)
    :returns: ParticleGun instance

    """

    from GaudiKernel.SystemOfUnits import GeV, rad
    from Configurables import ParticleGun
    pgun = ParticleGun("ParticleGun")
    pgun.EventType = 53210205

    from Configurables import MomentumRange
    pgun.addTool(MomentumRange, name="MomentumRange")
    pgun.ParticleGunTool = "MomentumRange"

    from Configurables import FlatNParticles
    pgun.addTool(FlatNParticles, name="FlatNParticles")
    pgun.NumberOfParticlesTool = "FlatNParticles"
    pgun.FlatNParticles.MinNParticles = 2
    pgun.FlatNParticles.MaxNParticles = 2
    pgun.MomentumRange.PdgCodes = [-13, 13]

    pgun.MomentumRange.MomentumMin = 2.0*GeV
    pgun.MomentumRange.MomentumMax = 100.0*GeV
    pgun.MomentumRange.ThetaMin = 0.015*rad
    pgun.MomentumRange.ThetaMax = 0.300*rad
    return pgun


def configure_generation(**kwargs):
    """Simple utility function to create and configure a Generation instance

    :**kwargs: Optional keyword arguments (not curently used)
    :returns: Generation instance

    """

    from Configurables import Generation, MinimumBias, Pythia8Production
    from Configurables import CollidingBeamsWithSvc
    from .Utilities import beaminfoService
    gen = Generation()
    # Only configure BeamInfoSvc here as pgun won't need it
    beaminfoService()

    mbias = gen.addTool(MinimumBias, name="MinimumBias")
    mbias.CutTool = ""
    pprod = gen.MinimumBias.addTool(Pythia8Production, name="Pythia8Production")
    gen.MinimumBias.ProductionTool = "Pythia8Production"
    pprod.addTool(CollidingBeamsWithSvc, name="CollidingBeamsWithSvc")
    pprod.BeamToolName = 'CollidingBeamsWithSvc'

    gen.PileUpTool = 'FixedLuminosityWithSvc'
    gen.VertexSmearingTool = 'BeamSpotSmearVertexWithSvc'

    gen.DecayTool = ""
    gen.MinimumBias.DecayTool = ""

    return gen


def configure_generationMT(**kwargs):
    """Simple utility function to create and configure a Generation instance

    :**kwargs: Optional keyword arguments (not curently used)
    :returns: Generation instance

    """

    from Configurables import Generation, MinimumBias, Pythia8ProductionMT
    from Configurables import CollidingBeamsWithSvc
    from .Utilities import beaminfoService
    gen = Generation()
    # Only configure BeamInfoSvc here as pgun won't need it
    beaminfoService()

    mbias = gen.addTool(MinimumBias, name="MinimumBias")
    mbias.CutTool = ""
    pprod = gen.MinimumBias.addTool(Pythia8ProductionMT,
                                    name="Pythia8ProductionMT")
    gen.MinimumBias.ProductionTool = "Pythia8ProductionMT"
    pprod.addTool(CollidingBeamsWithSvc, name="CollidingBeamsWithSvc")
    pprod.BeamToolName = 'CollidingBeamsWithSvc'
    from Configurables import Gaussino
    pprod.NThreads = Gaussino().ThreadPoolSize

    gen.PileUpTool = 'FixedLuminosityWithSvc'
    gen.VertexSmearingTool = 'BeamSpotSmearVertexWithSvc'

    gen.DecayTool = ""
    gen.MinimumBias.DecayTool = ""

    return gen


def configure_rnd_init(**kwargs):
    """Simple utility function to create and configure an instance GenRndInit

    :**kwargs: Optional keyword arguments (not curently used)
    :returns: GenRndInit

    """
    from Configurables import Gaussino
    conf = None
    if Gaussino().getProp("ReDecay"):
        from Configurables import GenReDecayInit
        conf = GenReDecayInit
        _name = kwargs['name'] if 'name' in kwargs else 'GenReDecayInit'
    else:
        from Configurables import GenRndInit
        conf = GenRndInit
        _name = kwargs['name'] if 'name' in kwargs else 'GenRndInit'

    from Configurables import SeedingTool
    conf(_name).addTool(SeedingTool, name='SeedingTool')
    return conf(_name)


def configure_gen_monitor(**kwargs):
    """Simple utility function to create and configure a GenMonitorAlg instance

    :**kwargs: Optional keyword arguments (not curently used)
    :returns: GenMonitorAlg instance

    """
    from Configurables import GenMonitorAlg

    TESLocation = "/Event/Gen/HepMCEvents"
    return GenMonitorAlg(
        "GenMonitorAlg",
        HistoProduce=True,
        Input=TESLocation
        )


def configure_hepmc_writer(**kwargs):
    """Simple utility function to create and configure a HepMCinstance

    :**kwargs: Optional keyword arguments (not curently used)
    :returns: GenMonitorAlg instance

    """
    from Configurables import HepMCWriter
    from Configurables import Gaussino

    alg = HepMCWriter()
    alg.Input = "/Event/Gen/HepMCEvents"
    filename = Gaussino().outputName() + '-HepMC'
    if hasattr(alg, 'Writer'):
        writer = alg.Writer
    else:
        writer = 'WriterRootTree'
    print('writer={}'.format(writer))
    if writer in ['WriterRootTree', 'WriterRoot']:
        print('Setting root file')
        alg.OutputFileName = filename + '.root'
    elif writer in ['WriterAscii']:
        alg.OutputFileName = filename + '.txt'
    elif writer in ['WriterHEPEVT']:
        alg.OutputFileName = filename + '.evt'
    else:
        print('Unknown writer name specified, not going to write')
        alg.OutputFileName = ''
    return alg
